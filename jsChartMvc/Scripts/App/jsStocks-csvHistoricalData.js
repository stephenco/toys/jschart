
var jsStocksCsvHistoricalData = function(){
    var self = this;
    var fn = { onData: [] };

    self.onData = function(callback) {
        // Register a new callback function to invoke when data is updated.
        fn.onData[fn.onData.length] = callback;
    };

    self.pull = function(fromTime, toTime) {

        if (!fromTime) { return; }

        // Convert/ensure times to moment() format for clarity.
        // This is not actually necessary but it clears up potential JS date confusion.
        if (!moment.isMoment(fromTime)) { fromTime = moment(fromTime); }
        if (!toTime) { toTime = moment(); }
        else if (!moment.isMoment(toTime)) { toTime = moment(toTime); }

        if (!toTime.isAfter(fromTime)) { return; }

        $.ajax({
            url: '/Charts/CsvHistoryServiceProxy',
            data: {
                a: fromTime.month(),
                b: fromTime.date(),
                c: fromTime.year(),
                d: toTime.month(),
                e: toTime.date(),
                f: toTime.year()
            }
        })
        .fail(function(xhr, err){ console.error("Request failed: " + err); })
        .done(function(result){

            // Translate the CSV information into something more useful and notify callbacks of updated data.
            var data = translateCsv(result);
            _.each(fn.onData, function(item) { item.call(this, data); });
        });
    };

    function translateCsv(csv) {
        var data = [];
        var map = { date: 0, open: 1, high: 2, low: 3, close: 4, volume: 4, adjClose: 5 };
        var lines = csv.split('\n');
        lines.shift();
        _.each(lines, function (line) {
            var vals = line.split(',');
            if (vals == "") return;

            data.push({
                x: moment(vals[map.date].trim(), 'YYYY-MM-DD').valueOf(),
                y: parseFloat(vals[map.close]),
                high: parseFloat(vals[map.high]),
                low: parseFloat(vals[map.low]),
            });
        });
        return data;
    }
};
