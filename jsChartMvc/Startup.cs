﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jsChartMvc.Startup))]
namespace jsChartMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
