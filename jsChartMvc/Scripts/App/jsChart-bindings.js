
ko.bindingHandlers.stockHistoryChart = {
    init: function(el, valueAccessor){
        var data = ko.utils.unwrapObservable(valueAccessor());
        var lows = _.map(data, function(item) { return { x: item.x, y: item.low }});
        var highs = _.map(data, function(item) { return { x: item.x, y: item.high }});
        var chart = new CanvasJS.Chart(el.id, {
            axisX:{ gridThickness: 1, gridColor: "#ddd" },
            axisY: { valueFormatString: "$#,###,##0.00", gridColor: "#ddd"/*, minimum: _.min(lows), maximum: _.max(highs)*/ },
            data: [{
                type: "line",
                lineThickness: 5,
                xValueType: "dateTime",
                dataPoints: data
            },{
                type: "line",
                lineThickness: 1,
                xValueType: "dateTime",
                dataPoints: highs
            },{
                type: "line",
                lineThickness: 1,
                xValueType: "dateTime",
                dataPoints: lows
            }]
        });
        chart.render();
        el.chart = chart;
    },
    update: function(el, valueAccessor){
        var data = ko.utils.unwrapObservable(valueAccessor());
        var lows = _.map(data, function (item) { return { x: item.x, y: item.low } });
        var highs = _.map(data, function (item) { return { x: item.x, y: item.high } });
        //el.chart.options.axisY.minimum = _.min(lows);
        //el.chart.options.axisY.maximum = _.max(highs);
        el.chart.options.data[0].dataPoints = data;
        el.chart.options.data[1].dataPoints = highs;
        el.chart.options.data[2].dataPoints = lows;
        el.chart.render();
    }
};

