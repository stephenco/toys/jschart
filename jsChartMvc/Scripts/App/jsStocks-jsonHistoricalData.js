
var jsStocksJsonHistoricalData = function(){
    var self = this;
    var fn = { onData: [] };

    self.onData = function(callback) {
        // Register a new callback function to invoke when data is updated.
        fn.onData[fn.onData.length] = callback;
    };

    self.pull = function(fromTime, toTime) {

        if (!fromTime) { return; }

        // Convert/ensure times to moment() format for clarity.
        // This is not actually necessary but it clears up potential JS date confusion.
        if (!moment.isMoment(fromTime)) { fromTime = moment(fromTime); }
        if (!toTime) { toTime = moment(); }
        else if (!moment.isMoment(toTime)) { toTime = moment(toTime); }

        if (!toTime.isAfter(fromTime)) { return; }

        var url = 'http://query.yahooapis.com/v1/public/yql';
        var data = encodeURIComponent('select * from yahoo.finance.historicaldata where symbol in ("GOOG") and startDate = "' + fromTime.format('YYYY-MM-DD') + '" and endDate = "' + toTime.format('YYYY-MM-DD') + '"');

        $.getJSON(url, 'q=' + data + "&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json", function (result) {
            var converted = _.map(result.query.results.quote, function (item) {
                return { x: moment(item.Date).valueOf(), y: parseFloat(item.Close), high: parseFloat(item.High), low: parseFloat(item.Low) };
            });
            _.each(fn.onData, function (fn) { fn.call(self, converted); });
            return converted;
        });
    };
};
