﻿using System.Web.Mvc;

namespace jsChartMvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}