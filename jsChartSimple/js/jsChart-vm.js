
var jsChartVm = function(dataSource, fnStartup){
    var self = this;

    // Underlying data values stored up over time; other properties will rely on this.
    self.data = ko.observable([]);
    self.data.subscribe(function(vals){
        var max = 100000;
        if (vals.length > max) {
            self.data(vals.slice(max * -1)); // Cap on how much historical data to keep around.
        }
    });

    // Other self.whatever properties are here to support nested subscriptions and UI concerns.
    // Making things ko.observable enables declarative markup binding and easy live updates.
    // Computed values are updated when their associated observables change. They simplify markup binding
    // and make it more model oriented (ie. less script in the view and more declarative language).

    self.historyScope = ko.observable('minutes');
    self.historyLength = ko.observable(5);

    self.toTime = ko.observable(moment());
    self.fromTime = ko.computed(function(){
        return moment(self.toTime()).subtract(self.historyScope(), self.historyLength());
    });

    self.updateFreqMs = ko.observable(0);
    self.samplesPerSecond = ko.observable(2);

    // This will be the window of data that actually gets bound to the UI.
    // It will be automatically updated based on subscriptions to other observable properties.
    self.selectedData = ko.computed(function(){
        if (!self.data().length) { return []; }

        var to = self.toTime().toDate();
        var from = self.fromTime().toDate();

        if (to == from) { console.log("to = from"); }

        return _.filter(self.data(), function(item) {
            var mx = moment(item.x).toDate();
            return mx <= to && mx >= from;
        });
    });

    self.selectedDataPoints = ko.computed(function() { return self.selectedData().length; });
    self.totalDataPoints = ko.computed(function() { return self.data().length; });

    // In a more complete implementation there would be checks here to ensure that dataSource is valid/usable.

    // Register for updates on the dataSource; push values into self.data when updates occur.
    dataSource.onData(function(data){
        if (!data.length) { return; }

        function sortPredicate(item) { return item.x; }

        data = _.sortBy(data, sortPredicate);

        // remove a slice of historical data if new data overlaps
        var first = data[0].x;
        var last = data[data.length-1].x;
        var original = _.filter(self.data(), function(val) {
            return val.x < first || val.x > last;
        });

        // update observable
        self.data(_.sortBy(original.concat(data), sortPredicate));
    });

    // Automatically pull updates from the dataSource based on the updateFreqMs value, when appropriate.
    var updateInterval = null;
    var lastUpdate = null;
    self.updateFreqMs.subscribe(function(val){
        if (updateInterval) { clearInterval(updateInterval); }
        if (val < 10) { return; }
        updateInterval = setInterval(function(){
            self.toTime(moment());
            var samples = (val / 1000) * self.samplesPerSecond();
            self.update(samples > 0 ? samples : 1);
        }, val);
    });

    self.historyLength.subscribe(function(){
        lastUpdate = null;
    });
    self.historyScope.subscribe(function(val){
        var samples = null;
        switch (val) {
            case 'years':
            case 'months':
                samples = 10000;
                self.updateFreqMs(0);
                break;
            case 'days':
            case 'hours':
                samples = 1000;
                self.updateFreqMs(0);
                break;
            case 'minutes':
                samples = self.samplesPerSecond() * 60;
                if (!self.updateFreqMs()) { self.updateFreqMs(3000); }
                break;
            case 'seconds':
                samples = self.samplesPerSecond();
                if (!self.updateFreqMs()) { self.updateFreqMs(1000); }
                break;
        }
        lastUpdate = null;
        self.update(samples);
    });

    // Update the data set to current information on demand.
    self.update = function(samples){
        self.toTime(moment());
        dataSource.pull(lastUpdate || self.fromTime(), self.toTime(), samples);
        lastUpdate = self.toTime();
    };

    if (fnStartup) {
        fnStartup.call(self);
    }
};
