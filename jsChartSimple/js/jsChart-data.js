
var jsChartData = function(){
    var self = this;
    var fn = { onData: [] };

    self.onData = function(callback) {
        // Register a new callback function to invoke when data is updated.
        fn.onData[fn.onData.length] = callback;
    };

    self.pull = function(fromTime, toTime, samples) {

        // A more complete implementation would perform a service call here, probably using jQuery.ajax().
        // However this is simply a very simple simulation creating a random data set between the times requested.
        var data = simulateData(fromTime, toTime, samples);

        // Notify callbacks and return data
        _.each(fn.onData, function(item) { item.call(this, data); });
        return data;
    };

    var lastSample = null;

    function simulateData(fromTime, toTime, samples) {
        if (!fromTime) { return []; }

        // Convert/ensure times to moment() format for clarity.
        // This is not actually necessary but it clears up potential JS date confusion.
        if (!moment.isMoment(fromTime)) { fromTime = moment(fromTime); }
        if (!toTime) { toTime = moment(); }
        else if (!moment.isMoment(toTime)) { toTime = moment(toTime); }

        if (!toTime.isAfter(fromTime)) { return []; }

        // Set the default number of samples to pull between times if needed.
        samples = samples || 100;

        var maxSamples = 10000;
        if (samples > maxSamples) { samples = maxSamples; }

        // Determine the space between samples and set up random values.
        var msBetweenSamples = Math.round((toTime.valueOf() - fromTime.valueOf()) / samples);
        var yVal = lastSample || Math.random() * 100;
        var data = [];

        // Start generating samples.
        for (var i = 0; i < samples; i++) {
            var vector = Math.random();
            if (Math.random() < .5) { vector *= -1; }

            yVal = yVal + vector;
            if (yVal < 0) { yVal = 0; }
            else if (yVal > 100) { yVal = 100; }

            // Add the sample record into data.
            data.push({
                x: fromTime.valueOf() + (i * msBetweenSamples),
                y: yVal
            });

            lastSample = yVal;
        }

        return data;
    }
};
