
var jsStocksLiveData = function(){
    var self = this;
    var fn = { onData: [] };

    self.onData = function(callback) {
        // Register a new callback function to invoke when data is updated.
        fn.onData[fn.onData.length] = callback;
    };

    self.pull = function() {

        var url = 'http://query.yahooapis.com/v1/public/yql';
        var data = encodeURIComponent('select * from yahoo.finance.quotes where symbol in ("GOOG")');

        $.getJSON(url, 'q=' + data + "&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json", function (result) {
            var quote = result.query.results.quote;
            var converted = { x: moment().valueOf(), y: parseFloat(quote.LastTradePriceOnly) }

            _.each(fn.onData, function (fn) { fn.call(self, [ converted ]); });
            return [ converted ];
        });

    };
};
