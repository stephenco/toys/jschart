
ko.bindingHandlers.chart = {
    init: function(el, valueAccessor){
        var chart = new CanvasJS.Chart(el.id, {
            zoomEnabled: true,
            panEnabled: true,
            title: { text: "Live Performance Data" },
            axisX:{ title: "Time", gridThickness: 1 },
            axisY: { title: "Values", minimum: 0, maximum: 100 },
            data: [{
                type: "line",
                xValueType: "dateTime",
                dataPoints: ko.utils.unwrapObservable(valueAccessor())
            }]
        });
        chart.render();
        el.chart = chart;
    },
    update: function(el, valueAccessor){
        el.chart.options.data[0].dataPoints = ko.utils.unwrapObservable(valueAccessor());
        el.chart.render();
    }
};

ko.bindingHandlers.stockHistoryChart = {
    init: function(el, valueAccessor){
        var data = ko.utils.unwrapObservable(valueAccessor());
        var chart = new CanvasJS.Chart(el.id, {
            zoomEnabled: true,
            panEnabled: true,
            title: { text: "Historical Stock Data" },
            axisX:{ title: "Time", gridThickness: 1 },
            axisY: { title: "Values", minimum: 0, maximum: 100 },
            data: [{
                type: "line",
                xValueType: "dateTime",
                dataPoints: data
            },{
                type: "line",
                xValueType: "dateTime",
                dataPoints: _.map(data, function(item) { return { x: item.x, y: item.high }})
            },{
                type: "line",
                xValueType: "dateTime",
                dataPoints: _.map(data, function(item) { return { x: item.x, y: item.low }})
            }]
        });
        chart.render();
        el.chart = chart;
    },
    update: function(el, valueAccessor){
        var data = ko.utils.unwrapObservable(valueAccessor());
        el.chart.options.data[0].dataPoints = data;
        el.chart.options.data[1].dataPoints = _.map(data, function(item) { return { x: item.x, y: item.high }});
        el.chart.options.data[2].dataPoints = _.map(data, function(item) { return { x: item.x, y: item.low }});
        el.chart.render();
    }
};

