﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Cache;
using System.Web.Mvc;

namespace jsChartMvc.Controllers
{
    public class ChartsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult JsonLive()
        {
            return View();
        }

        public ActionResult JsonHistory()
        {
            return View();
        }

        public ActionResult CsvHistory()
        {
            return View();
        }

        public ActionResult CsvHistoryServiceProxy(string a, string b, string c, string d, string e, string f)
        {
            using (var client = new WebClient() { CachePolicy = new RequestCachePolicy(RequestCacheLevel.BypassCache) })
            {
                var url = "http://ichart.yahoo.com/table.csv?s=GOOG" +
                    "&a=" + Url.Encode(a) + "&b=" + Url.Encode(b) + "&c" + Url.Encode(c) +
                    "&d=" + Url.Encode(d) + "&e=" + Url.Encode(e) + "&f=" + Url.Encode(f) +
                    "&g=d&ignore=.csv";
                var result = client.DownloadData(url);
                return new FileContentResult(result, "text/text");
            }
        }
    }
}